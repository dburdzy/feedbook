package repository;

import model.UserRoles;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Dominik on 22.05.15.
 */

public interface UserRolesRepository extends CrudRepository<UserRoles, Integer> {
}
