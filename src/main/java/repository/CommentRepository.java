package repository;

import model.Comment;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by Dominik on 04.05.15.
 */

public interface CommentRepository extends CrudRepository<Comment, Integer> {

    @Query(value = "select c from Comment c")
    List<Comment> selectAllPosts();
}
