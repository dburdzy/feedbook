package repository;

import model.Board;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by Dominik on 18.06.15.
 */

public interface BoardRepository extends CrudRepository<Board, Integer> {

    @Query(value = "select b from Board b where b.owner.id = ?1 order by b.whenAdded desc")
    List<Board> getListByOwnerId(Integer id);

    @Query(value = "select b from Board b order by b.whenAdded desc")
    List<Board> getListSortedByDate();

    @Query(value = "select b from Board b where lower(b.owner.username) like concat('%',lower(?1),'%')")
    List<Board> findByUsername(String name);

    @Query(value = "select b from Board b where b.id = ?1")
    Board findById(Integer id);

}