package repository;

import model.Feed;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by Dominik on 18.06.15.
 */

public interface FeedRepository extends CrudRepository<Feed, Integer> {

    @Query(value = "select f from Feed f where f.owner.id = ?1 order by f.whenAdded desc")
    List<Feed> getListByOwnerId(Integer id);

    @Query(value = "select f from Feed f order by f.whenAdded desc")
    List<Feed> getListSortedByDate();

    @Query(value = "select f from Feed f where lower(f.owner.username) like concat('%',lower(?1),'%')")
    List<Feed> findByUsername(String name);

    @Query(value = "select f from Feed f where f.id = ?1")
    Feed findById(Integer id);
}
