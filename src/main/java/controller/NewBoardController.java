package controller;

import model.Board;
import model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import service.BoardService;
import service.UserService;

import javax.validation.Valid;
import java.security.Principal;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Dominik on 18.06.15.
 */

@Controller
public class NewBoardController {

    @Autowired
    private UserService userService;

    @Autowired
    private BoardService boardService;

    @RequestMapping(value = "/newboard", method = RequestMethod.GET)
    public String index(ModelMap model, Principal principal)
    {
        User user = userService.getLoggedUser();
        model.addAttribute("user", user);
        model.addAttribute("username", user.getUsername());

        model.addAttribute("board", new Board());

        return "newBoard";
    }

    @RequestMapping(value = "/newboard", method = RequestMethod.POST)
    public String postNewBoard(@Valid Board board, BindingResult result, ModelMap model)
    {
        User user = userService.getLoggedUser();
        model.addAttribute("user", user);
        model.addAttribute("username", user.getUsername());
        model.addAttribute("board", board);

        if(result.hasErrors())
        {
            model.addAttribute("addBoardError", true);
            return "newBoard";
        }

        board.setOwner(user);
        board.setWhenAdded(new Date());
        boardService.addNewBoard(board);

        model.addAttribute("added", true);
        board.setDescription("");
        board.setBoardName("");

        return "newBoard";
    }
}
