package controller;

import model.Board;
import model.Feed;
import model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import service.BoardService;
import service.FeedService;
import service.UserService;

import javax.validation.Valid;
import java.util.Date;
import java.util.List;

/**
 * Created by Dominik on 18.06.15.
 */

@Controller
public class BoardController {

    @Autowired
    private BoardService boardService;

    @Autowired
    private FeedService feedService;

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/board/{boardId}", method = RequestMethod.GET)
    public String board(@PathVariable int boardId, ModelMap modelMap) throws Exception {
        if(!userService.isLogged()) {
            modelMap.addAttribute("userNotLogged", true);
        }
        Board board = boardService.getBoardWithId(boardId);
        if (board == null) {
            modelMap.addAttribute("notFound", true);
            return "board";
        }
        modelMap.addAttribute("board", board);
        List<Feed> feeds = board.getFeeds();
        modelMap.addAttribute("feeds", feeds);
        modelMap.addAttribute("feed", new Feed());
        return "board";
    }

    @RequestMapping(value = "/board/{boardId}", method = RequestMethod.POST)
    public String addFeed(@PathVariable int boardId, @Valid Feed feed,
                             BindingResult result, ModelMap modelMap) {
        if (result.hasErrors()) {
            return "board";
        }
        Board board = boardService.getBoardWithId(boardId);
        if (board == null) {
            return "board";
        }
        User user = userService.getLoggedUser();
        if (user == null) {
            return "board";
        }
        feed.setBoard(board);
        feed.setOwner(user);
        feed.setWhenAdded(new Date());
        feedService.addNewFeed(feed);
        return "redirect:/board/".concat(String.valueOf(boardId));
    }
}
