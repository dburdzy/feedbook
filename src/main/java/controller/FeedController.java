package controller;

import model.Comment;
import model.Feed;
import model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import service.CommentService;
import service.FeedService;
import service.UserService;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by Dominik on 18.06.15.
 */

@Controller
public class FeedController {

    @Autowired
    private FeedService feedService;

    @Autowired
    private CommentService commentService;

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/feed/{feedId}", method = RequestMethod.GET)
    public String feed(@PathVariable int feedId, ModelMap modelMap) throws Exception {
        if(!userService.isLogged()) {
            modelMap.addAttribute("userNotLogged", true);
        }
        Feed feed = feedService.getFeedWithId(feedId);
        if (feed == null) {
            modelMap.addAttribute("notFound", true);
            return "feed";
        }
        modelMap.addAttribute("feed", feed);
        List<Comment> comments = feed.getComments();
        modelMap.addAttribute("comments", comments);
        modelMap.addAttribute("comment", new Comment());
        return "feed";
    }

    @RequestMapping(value = "/feed/{feedId}", method = RequestMethod.POST)
    public String addComment(@PathVariable int feedId, @Valid Comment comment,
                             BindingResult result, ModelMap modelMap) {
        if (result.hasErrors()) {
            return "feed";
        }
        Feed feed = feedService.getFeedWithId(feedId);
        if (feed == null) {
            return "feed";
        }
        User user = userService.getLoggedUser();
        if (user == null) {
            return "feed";
        }
        comment.setFeed(feed);
        comment.setCommentAuthor(user);
        commentService.save(comment);
        return "redirect:/feed/".concat(String.valueOf(feedId));
    }
}
