package controller;

import model.Board;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import service.BoardService;

import java.security.Principal;
import java.util.List;

/**
 * Created by Dominik on 18.06.15.
 */

@Controller
public class ListBoardsController {

    @Autowired
    private BoardService boardService;

    @RequestMapping(value = "/listboards", method = RequestMethod.GET)
    public String dashboard(ModelMap model, Principal principal)
    {
        List<Board> myBoards = boardService.listNewestBoards();
        model.addAttribute("boards", myBoards);
        model.addAttribute("boardName", "ListBoards");
        return "listBoards";
    }
}
