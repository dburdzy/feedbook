package controller;

import model.User;
import org.springframework.stereotype.Controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import service.interfaces.IUserService;

import javax.validation.Valid;

/**
 * Created by Dominik on 22.05.15.
 */

@Controller
public class ProfileController {
    private IUserService userService;

    @Autowired
    public ProfileController(IUserService userService) {
        this.userService = userService;
    }

    @RequestMapping(value = "/profile/edit", method = RequestMethod.GET)
    public String index(Model model) {
        User user = userService.getLoggedUser();
        if (user == null) {
            model.addAttribute("error", "You're not logged in.");
            user = new User();
        }

        user.setPassword("");
        model.addAttribute("user", user);
        return "profile";
    }

    @RequestMapping(value = "/profile", method = RequestMethod.GET)
    public String profile(Model model) {
        User user = userService.getLoggedUser();
        if (user == null) {
            model.addAttribute("error", "You're not logged in.");
            user = new User();
        }

        user.setPassword("");
        model.addAttribute("user", user);
        model.addAttribute("editBtn", true);
        return "user-info";
    }

    @RequestMapping(value = "/profile", method = RequestMethod.POST)
    public String save(@Valid User user, BindingResult result, Model model) {
        if (result.hasErrors()) {
            model.addAttribute("error", "You have specified incorrect data.");
            return "profile";
        }

        if (userService.checkIfIdExists(user.getId())) {
            userService.updateUser(user);
        } else {
            model.addAttribute("error", "User does not exist.");
        }

        model.addAttribute("user", user);
        return "profile";
    }

    @RequestMapping(value = "/profile/changePassword", method = RequestMethod.POST)
    public String changePassword(@Valid User user, BindingResult result, Model model) {
        this.userService.changePassword(user);
        return this.index(model);
    }
}
