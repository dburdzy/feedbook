package controller;

import model.User;
import service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;
import java.util.List;

@Controller
public class UserController {

    @Autowired
    private UserService userService;
    
    @RequestMapping(value = "/user/{userId}", method = RequestMethod.GET)
    public ModelAndView renderRatings(@PathVariable("userId") Integer userId, Principal principal) {
        ModelAndView modelAndView = new ModelAndView("user-info");
        User viewedUser = userService.getById(userId);
        if (viewedUser == null) {
            modelAndView.addObject("error", String.format("User with ID %d was not found.", userId));
            return modelAndView;
        }
        modelAndView.addObject("viewedUser", viewedUser);

        return modelAndView;
    }
	
}