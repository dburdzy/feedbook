package controller;
 
import java.security.Principal;

import service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.inject.Inject;
import java.util.List;

/**
 * Created by Dominik on 22.05.15.
 */

@Controller
public class LoginController {

    @Autowired
    private UserService userService;

	@RequestMapping(value="/welcome", method = RequestMethod.GET)
	public String index(ModelMap model, Principal principal) {
        User user = (User) ((Authentication)principal).getPrincipal();
		String name = user.getUsername();
        String password = user.getPassword();
        model.User user1 = userService.getByName(name);
        model.addAttribute("user", user1);
		model.addAttribute("username", name);
        model.addAttribute("password", password);
        model.addAttribute("email", user1.getEmail());

		return "redirect:/listboards";
	}

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String indexRedirect(Principal principal) {
        if(principal == null)
            return "redirect:/login";
        else
            return "redirect:/welcome";
    }

	@RequestMapping(value="/login", method = RequestMethod.GET)
	public String login(ModelMap model) {
		return "login";
	}
	
	@RequestMapping(value="/loginfailed", method = RequestMethod.GET)
	public String loginError(ModelMap model) {
		model.addAttribute("error", "true");
		return "login";
	}
	
	@RequestMapping(value="/logout", method = RequestMethod.GET)
	public String logout(ModelMap model) {
        model.addAttribute("logout", "true");
		return "login";
	}
	
}