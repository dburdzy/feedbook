package service;

import model.Board;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.BoardRepository;
import service.interfaces.IBoardService;

import java.util.List;


/**
 * Created by Dominik on 18.06.15.
 */

@Service
public class BoardService implements IBoardService {

    @Autowired
    private BoardRepository boardRepository;

    @Override
    public void addNewBoard(Board board)   {
        boardRepository.save(board);
    }

    @Override
    public List<Board> listBoards(int id) {
        return this.boardRepository.getListByOwnerId(id);
    }

    @Override
    public Board getBoardWithId(Integer id) {
        return boardRepository.findById(id);
    }

    @Override
    public List<Board> listNewestBoards() {
        return this.boardRepository.getListSortedByDate();
    }
}
