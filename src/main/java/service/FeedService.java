package service;

import model.Feed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import repository.FeedRepository;
import service.interfaces.IFeedService;
import service.interfaces.IUserService;

import java.util.List;

/**
 * Created by Dominik on 18.06.15.
 */

@Service
public class FeedService implements IFeedService {

    @Autowired
    private FeedRepository feedRepository;

    @Autowired
    private IUserService userService;

    @Override
    public void addNewFeed(Feed feed)   {
        feedRepository.save(feed);
    }

    @Override
    public List<Feed> listFeeds(int id) {
        return this.feedRepository.getListByOwnerId(id);
    }

    @Override
    public Feed getFeedWithId(Integer id) {
        return feedRepository.findById(id);
    }

    @Override
    public List<Feed> listNewestFeeds() {
        return this.feedRepository.getListSortedByDate();
    }
}
