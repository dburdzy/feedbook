package service.interfaces;

import model.User;

/**
 * Created by Dominik on 22.05.15.
 */

public interface IUserService {
    User getLoggedUser();

    boolean isLogged();

    void updateUser(User updatedUser);

    boolean checkIfIdExists(int id);

    void changePassword(User userWithNewPassword);
}
