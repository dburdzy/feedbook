package service.interfaces;

import model.Feed;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * Created by Dominik on 18.06.15.
 */
public interface IFeedService {

    void addNewFeed(Feed feed);

    List<Feed> listFeeds(int id);

    Feed getFeedWithId(Integer id);

    List<Feed> listNewestFeeds();
}
