package service.interfaces;

import model.Comment;

import java.util.List;

/**
 * Created by Dominik on 04.05.15.
 */
public interface ICommentService {

    public Iterable<Comment> findAll();

    public void save(Comment comment);

    public List<Comment> selectAllPosts();
}
