package service.interfaces;

import model.Board;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.util.List;

/**
 * Created by Dominik on 18.06.15.
 */
public interface IBoardService {

    void addNewBoard(Board board);

    List<Board> listBoards(int id);

    Board getBoardWithId(Integer id);

    List<Board> listNewestBoards();
}
