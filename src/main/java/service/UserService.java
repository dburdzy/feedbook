package service;

import model.User;
import model.UserRoles;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import repository.UserRepository;
import repository.UserRolesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import service.interfaces.IUserService;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Set;

/**
 * Created by Dominik on 22.05.15.
 */

@Service
public class UserService implements IUserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserRolesRepository userRolesRepository;

    public void insertNewUser(User user) {
        user.setEnabled(true);
        UserRoles userRoles = new UserRoles(user.getId(), Roles.ROLE_USER.toString(), user);
        userRepository.save(user);
        userRolesRepository.save(userRoles);
    }

    public Iterable<UserRoles> findAllUserRoles() {
        return userRolesRepository.findAll();
    }

    public void deleteUserRole(UserRoles userRoles) {
        userRolesRepository.delete(userRoles);
    }

    public boolean checkIfUserIsInDatabase(String username) {
        List<User> users = selectAllUsers();
        for(User user : users) {
            if (user.getUsername().equals(username))
                return true;
        }
        return false;
    }

    public List<User> selectAllUsers() {
        return userRepository.selectAllUsers();
    }

    public User getByName(String name) {
        return userRepository.findByName(name);
    }

    public User getById(Integer id) {
        return userRepository.findById(id);
    }


    public void delete(Integer id) {
        userRepository.delete(id);
    }

    @Override
    public User getLoggedUser() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal == null) {
            return null;
        }

        org.springframework.security.core.userdetails.User user =
                (org.springframework.security.core.userdetails.User) principal;
        String username = user.getUsername();
        return this.getByName(username);
    }

    @Override
    public boolean isLogged() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String username = auth.getName();
        User user = this.getByName(username);
        if (user == null) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void updateUser(User updatedUser) {
        User userToUpdate = this.getById(updatedUser.getId());
        userToUpdate.setSurname(updatedUser.getSurname());
        userToUpdate.setEmail(updatedUser.getEmail());
        userToUpdate.setFirstName(updatedUser.getFirstName());
        userRepository.save(userToUpdate);
    }

    @Override
    public boolean checkIfIdExists(int id) {
        return userRepository.exists(id);
    }

    @Override
    public void changePassword(User userWithNewPassword) {
        User userToUpdate = this.getById(userWithNewPassword.getId());
        userToUpdate.setPassword(userWithNewPassword.getPassword());
        userRepository.save(userToUpdate);
    }
}

enum Roles {
    ROLE_USER("ROLE_USER"), ROLE_ADMIN("ROLE_ADMIN");

    String role;

    Roles(String role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return role;
    }
}
