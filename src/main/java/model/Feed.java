package model;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Created by Dominik on 18.06.15.
 */

@Entity
@Table(name = "feeds")
public class Feed {

    private Integer id;

    @Size(max = 10000, message = "Feed too long")
    private String content;

    private User owner;

    private Board board;

    private List<Comment> comments;

    private Date whenAdded;


    @Id
    @GeneratedValue
    @Column(name = "FEED_ID", nullable = false)
    public Integer getId() {
        return id;
    }

    @ManyToOne
    @JoinColumn(name = "USER_ID")
    public User getOwner() {
        return owner;
    }

    @ManyToOne
    @JoinColumn(name="BOARD_ID")
    public Board getBoard() {
        return board;
    }

    @OneToMany(mappedBy = "feed", fetch = FetchType.EAGER)
    @Fetch(value = FetchMode.SUBSELECT)
    public List<Comment> getComments() {
        return comments;
    }

    @Column(name = "CONTENT", length=10000, nullable = true)
    public String getContent() {
        return content;
    }

    @DateTimeFormat(pattern = "yyyy/mm/dd h:mm:ss")
    public Date getWhenAdded() {
        return whenAdded;
    }

    public void setWhenAdded(Date whenAdded) {
        this.whenAdded = whenAdded;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public void setBoard(Board board) {
        this.board = board;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Feed feed = (Feed) o;

        return !(id != null ? !id.equals(feed.id) : feed.id != null);

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
