package model;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;

/**
 * Created by Dominik on 18.06.15.
 */

@Entity
@Table(name = "boards")
public class Board {

    private Integer id;

    @Size(max = 10000, message = "Description too long")
    private String description;

    @Size(min = 3, max = 30, message = "Name must include 3 to 30 characters")
    private String boardName;

    private User owner;

    private List<Feed> feeds;

    private Date lastUpdateDate;

    private Date whenAdded;

    @Id
    @GeneratedValue
    @Column(name = "BOARD_ID", nullable = false)
    public Integer getId() {
        return id;
    }

    @ManyToOne
    @JoinColumn(name = "USER_ID")
    public User getOwner() {
        return owner;
    }

    @OneToMany(mappedBy = "board", fetch = FetchType.EAGER)
    public List<Feed> getFeeds() {
        return feeds;
    }

    @Column(name = "DESCRIPTION", length=10000, nullable = true)
    public String getDescription() {
        return description;
    }

    @Column(name = "BOARD_NAME", length=30, nullable = true)
    public String getBoardName() {
        return boardName;
    }

    @Column(name = "LAST_UPDATE_DATE")
    public Date getLastUpdateDate() {
        return lastUpdateDate;
    }


    @DateTimeFormat(pattern = "yyyy/mm/dd h:mm:ss")
    public Date getWhenAdded() {
        return whenAdded;
    }


    public void setWhenAdded(Date whenAdded) {
        this.whenAdded = whenAdded;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setFeeds(List<Feed> feeds) {
        this.feeds = feeds;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setBoardName(String boardName) {
        this.boardName = boardName;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public void setLastUpdateDate(Date lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    @PreUpdate
    @PrePersist
    private void onUpdate() {
        this.lastUpdateDate = new Date();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Board board = (Board) o;

        return !(id != null ? !id.equals(board.id) : board.id != null);

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
