package model;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Required;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by Dominik on 22.05.15.
 */

@Entity
@Table(name = "users")
public class User {

    private Integer id;

    @Size(min = 3, max = 20, message = "Username must consist of 3 to 20 characters.")
    @Pattern(regexp = "^[a-zA-Z0-9]+$", message = "Username must consist of alphanumeric characters with no spaces.")
    private String username;
    @Size(min = 4, max = 20, message = "Password cannot be shorter than 4 and longer than 20 characters.")
    private String password;
    @Pattern(regexp = "[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}", message = "Invalid email address.")
    @NotEmpty
    private String email;

    private boolean enabled;

    @Size(min = 2, max = 20, message = "First name must consist of 3 to 20 characters")
    private String firstName;
    @Size(min = 2, max = 50, message = "Surname name must consist of 2 to 50 characters")
    private String surname;

    @Id
    @GeneratedValue
    @Column(name = "USER_ID", nullable = false)
    public Integer getId() {
        return id;
    }

    @Column(name = "USERNAME", unique = true, nullable = false)
    public String getUsername() {
        return username;
    }

    @Column(name = "PASSWORD", nullable = false)
    public String getPassword() {
        return password;
    }

    @Column(name = "EMAIL", nullable = false)
    public String getEmail() {
        return email;
    }

    @Column(name = "ENABLED", nullable = false)
    public boolean getEnabled() {
        return enabled;
    }

    @Column(name = "FIRSTNAME", nullable = true)
    public String getFirstName() { return firstName; }

    @Column(name = "SURNAME", nullable = true)
    public String getSurname() { return surname; }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setSurname(String surname) { this.surname = surname; }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public boolean equals(Object object) {
        if(object == null){
            return false;
        }
        if(object.getClass() != User.class){
            return false;
        }
        User user = (User) object;
        return getUsername().equals(user.getUsername());
    }
}
