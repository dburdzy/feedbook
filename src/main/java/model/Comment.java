package model;

import javax.persistence.*;

/**
 * Created by Dominik on 04.05.15.
 */

@Entity
@Table(name = "comments")
public class Comment {

    private long id;

    private String commentContent;

    private User commentAuthor;

    private Feed feed;

    public Comment() {}

    public Comment(User commentAuthor, String commentContent, Feed feed) {
        this.commentAuthor = commentAuthor;
        this.commentContent = commentContent;
        this.feed = feed;
    }

    @Id
    @GeneratedValue
    @Column(name = "COMMENT_ID", nullable = false)
    public long getId() {
        return id;
    }

    @ManyToOne
    @JoinColumn(name = "USER_ID")
    public User getCommentAuthor() {
        return commentAuthor;
    }

    @Column(name = "CONTENT", length = 255)
    public String getCommentContent() {
        return commentContent;
    }

    @ManyToOne
    @JoinColumn(name="FEED_ID")
    public Feed getFeed() {
        return feed;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setCommentContent(String commentContent) {
        this.commentContent = commentContent;
    }

    public void setCommentAuthor(User commentAuthor) {
        this.commentAuthor = commentAuthor;
    }

    public void setFeed(Feed feed) {
        this.feed = feed;
    }
}