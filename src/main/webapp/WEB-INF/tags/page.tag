<%@tag description="Page template" pageEncoding="UTF-8"%>
<%@attribute name="head" fragment="true" %>
<%@attribute name="header" fragment="true" %>
<!DOCTYPE html>
<html>
<head>
    <%@include file="/WEB-INF/pages/template/head.jsp" %>
    <jsp:invoke fragment="head"/>
</head>
<body>
    <%@include file="/WEB-INF/pages/template/header.jsp" %>
    <div class="container-fluid">
        <div class="row row-offcanvas row-offcanvas-left">
            <%@include file="/WEB-INF/pages/template/sidebar.jsp" %>

            <div class="col-sm-9 col-md-10 main">
                <jsp:doBody />
            </div>

            <footer class="footer">
                <div class="container">
                    <p class="pull-right text-muted">©2015 FeedBook</p>
                </div>
        </footer>
        </div>
    </div>
    <script>
        $('ul.nav a').filter(function () {
            return this.href == location.href;
        }).parent().addClass('active').css('font-weight', 'bold');
    </script>
</body>
</html>
