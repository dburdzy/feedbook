<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:page>
    <jsp:attribute name="head">
        <script type="text/javascript" src='http://maps.google.com/maps/api/js?sensor=false&libraries=places'></script>
        <script src="<c:url value="/js/locationpicker.jquery.js" />"></script>
        <script>
            $(document).ready(function() {
                var radius = ${ad.location.radius};
                var latitude = ${ad.location.height};
                var longitude = ${ad.location.width};


                $('#locationPicker').locationpicker({
                    location: {latitude: latitude, longitude: longitude},
                    radius: radius,
                    draggable: false
                });
            });
        </script>
    </jsp:attribute>
  <jsp:body>
      <div class="page-header">
          <h3>${ad.title}</h3>
      </div>
      <div class="row">
          <div class="col-md-10">
              <c:choose>
                  <c:when test="${not empty notFound}">
                      <div class="alert alert-danger">
                          No ad with this ID found.
                      </div>
                  </c:when>
                  <c:otherwise>
                      <div class="panel panel-default">
                          <div class="panel-heading">
                              Posted by <a href="<c:url value="/user/${ad.owner.id}"/>">${ad.owner.username}</a>
                              <p class="pull-right"><b>Added: </b><fmt:formatDate pattern="dd/MM/yyyy HH:mm" value="${ad.whenAdded}" /></p>
                          </div>
                          <div class="panel-body">
                              <p><b>Description: </b></p>

                            ${ad.page}
                          </div>
                          <ul class="list-group">
                              <li class="list-group-item ">
                                  <b>Location: </b>
                                  <div class="center-block" id="locationPicker" style="width: 500px; height: 400px;"></div>
                              </li>
                              <li class="list-group-item">
                                  <b>Tags: &nbsp;</b>
                                  <c:forEach items ="${ad.tags}" var="tag">
                                      <span style="font-size: small;" class="label label-primary">${tag}</span>
                                  </c:forEach>
                              </li>
                          </ul>
                          <div class="panel-footer text-center">
                            <b style="font-size: medium;">Comments</b>
                          </div>
                          <ul class="list-group">
                              <c:if test="${empty comments}">
                                  <li class="list-group-item text-center">
                                      <b>No comments</b>
                                  </li>
                              </c:if>
                              <c:if test="${not empty comments}">
                                  <c:forEach var="comment" items="${comments}">
                                      <li class="list-group-item">
                                              <b><a href="<c:url value="/user/${comment.commentAuthor.id}"/>">${comment.commentAuthor.username}</a>: </b>
                                              ${comment.commentContent}
                                      </li>
                                  </c:forEach>
                              </c:if>

                              <c:choose>
                                  <c:when test="${empty userNotLogged}">
                                      <c:if test="${empty notFound}">
                                          <sf:form class="form-horizontal" method="POST" modelAttribute="comment" action="/ad/${ad.id}">
                                              <li class="list-group-item">
                                                  <div class="row">
                                                  <div class="col-md-10">
                                                      <sf:input path="commentContent" id="commentContent" class="form-control" placeholder="Enter your comment"/>
                                                      <sf:errors path="commentContent" cssStyle="color: #ff0000"/>
                                                  </div>
                                                  <div class="col-sm-2 text-center">
                                                      <button type="submit" class="btn btn-md btn-success">Submit comment</button>
                                                  </div>
                                                  </div>
                                              </li>
                                          </sf:form>
                                      </c:if>
                                  </c:when>
                                  <c:otherwise>
                                      <li class="list-group-item text-center" id="commentsForNotLoggedIn">
                                          <span style="font-size: medium;"><a href="<c:url value="/login"/>">Log in</a> to post a comment</span>
                                      </li>
                                  </c:otherwise>
                              </c:choose>
                          </ul>
                      </div>
                      <p><b>Tags: </b>
                          <c:forEach items ="${ad.tags}" var="tag">
                            <span class="label label-default">${tag}</span>
                          </c:forEach>
                      </p>
                  </c:otherwise>
              </c:choose>
          </div>
      </div>

      <c:choose>
          <c:when test="${empty userNotLogged}">
              <div class="container-fluid" id="comments">
                  <c:if test="${empty notFound}">
                      <sf:form class="form-horizontal" method="POST" modelAttribute="comment" action="/ad/${ad.id}">
                          <div class="row">
                              <div class="form-group">
                                  <div class="col-md-8">
                                      <sf:input path="commentContent" id="commentContent" class="form-control"
                                                placeholder=" Enter your comment here..." cssStyle="border: 2px solid dodgerblue; height: 100px;"/>
                                      <sf:errors path="commentContent" cssStyle="color: #ff0000"/>
                                  </div>
                              </div>
                              <div class="col-sm-3">
                                  <br/>
                                  <button type="submit" class="btn btn-md btn-success">Submit comment</button>
                              </div>
                          </div>
                      </sf:form>
                      <br/>
                      <div class="row">
                          <h3>Comments:</h3>
                      </div>
                      <c:if test="${not empty comments}">
                          <c:forEach var="comment" items="${comments}">
                              <div class="row">
                                  <div class="col-md-8" style="background-color: skyblue; border: 2px solid dodgerblue; border-radius: 10px;">
                                      <p><span class="author" style="font-weight: bold;">${comment.commentAuthor.username}</span></p>
                                      <p>${comment.commentContent}</p>
                                  </div>
                              </div>
                              <br/>
                          </c:forEach>
                      </c:if>
                  </c:if>
              </div>
          </c:when>
          <c:otherwise>
              <div class="container-fluid" id="commentsForNotLoggedIn">
                  <div class="row">
                      <h5>Log in to post a comment</h5>
                  </div>
              </div>
          </c:otherwise>
      </c:choose>

  </jsp:body>
</t:page>
