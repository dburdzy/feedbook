<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <%@include file="head.jsp" %>
</head>
<body>
<%@include file="header.jsp" %>

<div class="container">
    <br/>
    <br/>
    <div class="row">
        <div class="col-md-10">
            <h2 class="form-signout-heading ">Your feeds</h2>
            <br/>
            <ul class="list-group">
                <c:forEach items="${feeds}" var="feed">
                    <li class="list-group-item">${feed.text}</li>
                </c:forEach>
            </ul>
        </div>
    </div>
</div>

</body>
</html>
