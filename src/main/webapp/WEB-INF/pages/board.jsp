<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<sec:authorize var="loggedIn" access="isAuthenticated()"/>
<c:choose>
    <c:when test="${loggedIn}">
        <t:page>
            <jsp:attribute name="head">
                <link href="/css/summernote.css" rel="stylesheet">
                <script type="text/javascript" src="/js/summernote.min.js"></script>
                <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
                <script type="text/javascript" src='http://maps.google.com/maps/api/js?sensor=false&libraries=places'></script>
                <script src="<c:url value="/js/locationpicker.jquery.js" />"></script>
                <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
            </jsp:attribute>
            <jsp:body>
                <div class="page-header">
                    <h3>${board.boardName}</h3>
                </div>
                <div class="row">
                    <div class="col-md-10">
                        <c:choose>
                            <c:when test="${not empty notFound}">
                                <div class="alert alert-danger">
                                    No board with this ID found.
                                </div>
                            </c:when>
                            <c:otherwise>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        Posted by <a href="<c:url value="/user/${board.owner.id}"/>">${board.owner.username}</a>
                                        <p class="pull-right"><b>Added: </b><fmt:formatDate pattern="dd/MM/yyyy HH:mm" value="${board.whenAdded}" /></p>
                                    </div>
                                    <div class="panel-body">
                                        <p><b>Description: </b></p>
                                            ${board.description}
                                    </div>

                                    <c:if test="${empty notFound}">
                                        <sf:form id="newfeedForm" class="form-horizontal" method="POST"
                                                 modelAttribute="feed" action="/board/${board.id}">
                                            <div class="form-group">
                                                <div class="col-md-12">
                                                    <sf:input path="content" type="hidden" id="feedContent"/>
                                                    <div id="summernote">${feed.content}</div>
                                                </div>
                                            </div>
                                        </sf:form>
                                    </c:if>
                                    <div class="row-fluid">
                                        <button class="btn btn-default  btn-lg btn-primary center-block" id="addFeedButton">Submit Feed</button>
                                        <br/>
                                        <br/>
                                    </div>

                                    <div class="panel-footer text-center">
                                        <b style="font-size: medium;">Feeds</b>
                                    </div>
                                    <ul class="list-group">
                                        <c:if test="${empty feeds}">
                                            <li class="list-group-item text-center">
                                                <b>No feeds</b>
                                            </li>
                                        </c:if>
                                        <c:if test="${not empty feeds}">
                                            <c:forEach items="${feeds}" var="feed">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <div class="row">
                                                            <div class="col-md-2">
                                                                <a href="/user/${feed.owner.id}"><c:out value="${feed.owner.username}" /></a>
                                                            </div>
                                                            <div class="col-md-2 text-right">
                                                                <fmt:formatDate pattern="dd/MM/yyyy HH:mm" value="${feed.whenAdded}" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="panel-body">
                                                        <c:out value="${feed.content}" escapeXml="false" />
                                                    </div>
                                                    <ul class="list-group">
                                                        <li class="list-group-item">
                                                            <p class="pull-right"><a href="/feed/${feed.id}">Read more...</a></p>
                                                            <br/>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <br/>
                                            </c:forEach>
                                        </c:if>
                                    </ul>
                                </div>

                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>

                <script>
                    $(document).ready(function() {
                        $('#summernote').summernote({
                            height: 150,
                            maxHeight: 300
                        });
                    });

                    $("#addFeedButton").click(function() {
                        $("#feedContent").val($("#summernote").code());
                        $("#newfeedForm").submit();
                    });
                </script>
            </jsp:body>
        </t:page>
    </c:when>
    <c:otherwise>
        <%@include file="login.jsp" %>
    </c:otherwise>
</c:choose>