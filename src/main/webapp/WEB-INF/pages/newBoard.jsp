<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>


<sec:authorize var="loggedIn" access="isAuthenticated()"/>
<c:choose>
    <c:when test="${loggedIn}">
        <t:page>
            <jsp:attribute name="head">
                <link href="/css/summernote.css" rel="stylesheet">
                <script type="text/javascript" src="/js/summernote.min.js"></script>
                <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
                <script type="text/javascript" src='http://maps.google.com/maps/api/js?sensor=false&libraries=places'></script>
                <script src="<c:url value="/js/locationpicker.jquery.js" />"></script>
                <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
            </jsp:attribute>
            <jsp:body>
                <div class="page-header">
                    <h3>New Board</h3>
                </div>
                <c:if test="${not empty added}">
                    <div class="alert alert-success">
                        New board has been added.
                    </div>
                </c:if>

                <div class="container-fluid">
                    <sf:form id='newboardForm' action="/newboard" method='POST' class="form-horizontal" role="form"
                             modelAttribute="board">
                        <div class="row">
                            <c:if test="${not empty addBoardError}">
                                <div class="alert alert-danger">
                                    <sf:errors path="*"/>
                                </div>
                            </c:if>
                            <div class="form-group">
                                <div class="col-md-10">
                                    <label for="inputBoardName">Name</label>
                                    <sf:input path="boardName" class="form-control" id="inputBoardName" type="text"
                                              placeholder="Enter Board Name"/>
                                    <sf:errors path="boardName" cssStyle="color: #ff0000"/>
                                </div>
                            </div>
                            <br/>
                            <div class="form-group">
                                <div class="col-md-10">
                                    <label for="boardDescription">Board description</label>
                                    <sf:input path="description" type="hidden" id="boardDescription"/>
                                    <div id="summernote">${board.description}</div>
                                </div>
                            </div>
                        </div>
                    </sf:form>
                </div>
                <div class="row">
                    <br />
                    <button class="btn btn-default  btn-lg btn-primary" id="addBoardButton">Add</button>
                </div>

                <script>
                    $(document).ready(function() {
                        $('#summernote').summernote({
                            height: 300,
                            maxHeight: 500
                        });
                    });

                    $("#addBoardButton").click(function() {
                        $("#boardDescription").val($("#summernote").code());
                        $("#newboardForm").submit();
                    });
                </script>
            </jsp:body>
        </t:page>
    </c:when>
    <c:otherwise>
        <%@include file="login.jsp" %>
    </c:otherwise>
</c:choose>