<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<div class="col-sm-3 col-md-2 sidebar-offcanvas" id="sidebar" role="navigation">

  <ul class="nav nav-pills nav-stacked">
    <sec:authorize var="loggedIn" access="isAuthenticated()">
        <h5 class="page-header lead">&emsp;Profile Overview</h5>
        <li><a href="/profile">Profile</a></li>
        <li><a href="/profile/edit">Edit info</a></li>
        <li class="divider"></li>
    </sec:authorize>
    <h5 class="page-header lead" >&emsp;Boards</h5>
    <sec:authorize var="loggedIn" access="isAuthenticated()"><li><a href="/newboard">Create new board</a></li></sec:authorize>
    <li><a href="/listboards">Recently posted</a></li>
    <li class="dropdown">
      <a class="dropdown-toggle" data-toggle="dropdown" href="#">
        <span class="caret"></span>
      </a>
      <ul class="dropdown-menu">
        <li><a href="/listboards">Browse</a></li>
      </ul>
    </li>
    <li class="divider"></li>
    <br/>
    <li class="active">
      <c:choose>
        <c:when test="${loggedIn}">
          <form id="logout" action="<c:url value="/j_spring_security_logout"/>" method="POST">
            <button type="submit" class="btn btn-md btn-default btn-block" >Log out</button>
          </form>
        </c:when>
        <c:otherwise>
          <a href="/login" class="btn btn-md btn-default btn-block">Log in</a>
        </c:otherwise>
      </c:choose>
    </li>
  </ul>
  <hr>
</div>