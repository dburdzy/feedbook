<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<sec:authorize var="loggedIn" access="isAuthenticated()"/>
<nav class="navbar navbar-default navbar-fixed-top navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
        <c:choose>
            <c:when test="${loggedIn}">
                <a class="navbar-brand" href="/welcome" style="color: #ffffff"><strong>FeedBook</strong></a>
            </c:when>
            <c:otherwise>
                <a class="navbar-brand" href="/login" style="color: #ffffff"><strong>FeedBook</strong></a>
            </c:otherwise>
        </c:choose>
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li>&emsp;&emsp;&emsp;</li>
        <c:choose>
          <c:when test="${loggedIn}">
            <li><a href="/newboard" style="font-size: 150%;">Create new board</a></li>
          </c:when>
          <c:otherwise>
            <li><a href="/login" >Create new board</a></li>
          </c:otherwise>
        </c:choose>
        <li>&emsp;&emsp;</li>
        <li>&emsp;&emsp;</li>
        <li>&emsp;&emsp;</li>
        <li>&emsp;&emsp;</li>
      </ul>

      <sec:authorize access="isAuthenticated()">
        <div class="navbar-collapse collapse" style="margin-right:15px;">
          <form class="navbar-form navbar-right" action="<c:url value="/j_spring_security_logout"/>" method="POST">
            <div class="form-group text-muted" style="color: #ffffff">
              Logged in as: <strong><a href="<c:url value="/profile"/>" style="color: #ffffff"><sec:authentication property="principal.username" /></a> &nbsp</strong>
            </div>
            <button type="submit" class="btn btn-primary active">Log out</button>
          </form>
        </div>
      </sec:authorize>
    </div>
  </div>
</nav>
<br/>