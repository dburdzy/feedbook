<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>FeedBook<c:if test="${not empty username}">: ${username}</c:if></title>

<!-- Bootstrap core CSS -->
<link href="<c:url value="/css/better_bootstrap.css"/>" rel="stylesheet">
<!-- Awesome fonts -->
<link href="http://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
<!--link href="http://netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet"-->
<link href="/css/style.css" rel="stylesheet">
<script src="<c:url value="/js/jquery-2.1.3.js" />"></script>
<script src="<c:url value="/js/bootstrap.js" />"></script>