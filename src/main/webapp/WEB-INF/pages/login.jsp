<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:page>
  <jsp:attribute name="head">

  </jsp:attribute>
    <jsp:body>
        <div class="page-header">
            <h3>Sign in</h3>
        </div>
        <script>
            window.onload = function () {
                document.f.j_username.focus();
            }
        </script>
        <form name='f' action="<c:url value="/j_spring_security_check"/>"
              method='POST' class="form-signin form-horizontal" role="form">
            <!-- informacja o bledzie wynikajacym z niepodania loginu i/lub hasla -->
            <c:if test="${not empty error}">
                <div class="alert alert-danger">
                    Your login attempt was not successful, try again.<br/> Caused :
                        ${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}
                </div>
            </c:if>
            <!-- informacja o pomyslnym wylogowaniu sie -->
            <c:if test="${not empty logout}">
                <div class="alert alert-success">
                    Successfully logged out.
                </div>
            </c:if>

            <div class="row">

                <div class="col-md-10">

                    <div class="form-group">
                        <div class="col-md-4">
                            <label for="j_username">Login:</label>
                            <input type='text' class="form-control" id="j_username" name='j_username'>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-4 ">
                            <label for="j_password">Password:</label>
                            <input type='password' class="form-control" id="j_password" name='j_password' />
                            <br />
                            <input id="checkbox" type="checkbox" value="remember-me" />
                            <label for="checkbox">Remember me</label>
                        </div>
                    </div>
                    <br/>
                    <button class="btn btn-default btn-lg btn-primary " type="submit">Log in</button>
                    <br/><br/>
                    Don't have an account yet? <a href="<c:url value="/register"/>">Sign up now!</a>

                </div>
            </div>
        </form>
    </jsp:body>
</t:page>

