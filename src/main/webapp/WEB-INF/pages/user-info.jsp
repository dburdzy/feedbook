<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:page>
  <jsp:body>
      <h3 class="page-header">Contact info</h3>
    <div class="row">
      <div class="col-md-10">
        <c:choose>
          <c:when test="${not empty error}">
            <div class="alert alert-danger"> ${error} </div>
          </c:when>
          <c:otherwise>
            <div class="panel panel-primary">
              <div class="panel-heading lead">
                 <strong>${user.username}</strong>
              </div>
              <div class="panel-body">
                <p class="lead"><b>Name:</b>  ${user.firstName} ${user.surname} </p>
                <p class="lead"><b>Email:</b>  ${user.email}  </p>
              </div>
            </div>
          </c:otherwise>
        </c:choose>
      </div>
    </div>
    <c:choose>
      <c:when test="${editBtn}">
        <a class="btn  btn-lg btn-primary" type="button" href="/profile/edit">Edit</a>
      </c:when>
    </c:choose>
  </jsp:body>
</t:page>

