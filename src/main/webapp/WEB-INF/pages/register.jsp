<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:page>
    <jsp:body>
        <script>
            window.onload = function () {
                document.f.j_username.focus();
            }
        </script>
        <sf:form class="form-signout form-horizontal" method="POST" modelAttribute="user">
            <c:if test="${not empty addUserError}">
                <div class="alert alert-danger">
                    This login currently exists in database! Choose another one.<br/>
                </div>
            </c:if>
            <div class="row ">
                <div class="col-md-10">
                    <div class="page-header">
                        <h3>Sign up</h3>
                    </div>
                    <br/>
                    <div class="form-group">
                        <div class="col-md-5">
                            <label for="login">
                                Login (required):
                            </label>
                            <sf:input path="username" class="form-control" id="login" placeholder="Pick a login"/>
                            <sf:errors path="username" cssStyle="color: #ff0000" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-5">
                            <label for="password">
                                Password (required):
                            </label>
                            <sf:password path="password" class="form-control" id="password" placeholder="Create a password"/>
                            <sf:errors path="password" cssStyle="color: #ff0000"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-5">
                            <label for="email">
                                Email (required):
                            </label>
                            <sf:input path="email" class="form-control" id="email" placeholder="Enter your Email"/>
                            <sf:errors path="email" cssStyle="color: #ff0000"/>
                        </div>
                    </div>

                    <div class="row">
                        <br/>
                        <div class="col-md-4">
                            <button type="submit" class="btn btn-lg btn-success">
                                Register
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </sf:form>
    </jsp:body>
</t:page>
