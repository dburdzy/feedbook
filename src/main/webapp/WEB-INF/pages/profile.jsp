<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>


<sec:authorize var="loggedIn" access="isAuthenticated()"/>
<c:choose>
    <c:when test="${loggedIn}">
        <t:page>
          <jsp:body>
              <h3 class="page-header">Edit your profile</h3>
             <div class="row-fluid">
                <div class="col-md-6">
                    <h4 class="page-header">Personal info:</h4>
                    <sf:form class="form-horizontal" action="/profile" modelAttribute="user" method='POST'>
                        <c:if test="${not empty error}">
                            <div class="alert alert-danger">
                                ${error}
                            </div>
                        </c:if>

                        <sf:input path="username" type="hidden" />
                        <sf:input path="id" type="hidden" />

                        <div class="form-group">
                            <div class="col-md-6">
                                <label for="firstName">First name:</label>
                                <sf:input path="firstName" id="firstName" class="form-control" placeholder="First name"/>
                                <sf:errors path="firstName" cssStyle="color: #ff0000" />
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6">
                                <label for="surname">Last name:</label>
                                <sf:input path="surname" id="surname" class="form-control" placeholder="Last name" />
                                <sf:errors path="surname" cssStyle="color: #ff0000" />
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6">
                                <label for="email">Email:</label>
                                <sf:input path="email" id="email" class="form-control" placeholder="Email"/>
                                <sf:errors path="email" cssStyle="color: #ff0000" />
                            </div>
                        </div>
                        <div class="row">
                            <br/>
                            <div class="col-sm-2">
                                <sf:button class="btn btn-md btn-primary" type="submit">Save</sf:button>
                            </div>
                        </div>
                    </sf:form>
                </div>
            <div class="col-md-6">
                <h4 class="page-header">Change password:</h4>
                <br/>
                <sf:form class="form-horizontal" action="/profile/changePassword" modelAttribute="user" method='POST'>
                    <sf:input path="username" type="hidden" />
                    <sf:input path="id" type="hidden" />
                    <div class="form-group">
                        <div class="col-md-6">
                            <label for="password">Password:</label>
                            <sf:input path="password" id="password" type="password" class="form-control" placeholder="Enter new password" />
                            <sf:errors path="password" cssStyle="color: #ff0000" />
                        </div>
                    </div>
                    <div class="row">
                        <br/>
                        <div class="col-md-3">
                            <sf:button class="btn btn-md btn-primary " type="submit">Save</sf:button>
                        </div>
                        <br/><br/><br/>
                    </div>
                </sf:form>
            </div>
          </div>
          </jsp:body>
        </t:page>
    </c:when>
    <c:otherwise>
        <%@include file="login.jsp" %>
    </c:otherwise>
</c:choose>
