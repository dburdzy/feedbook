<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <%@include file="head.jsp" %>
</head>
<body>
<%@include file="header.jsp" %>

<div class="container">
    <br/>
    <br/>
    <div class="row">
        <div class="col-md-10">
            <h2 class="form-signout-heading ">Feed</h2>
            <br/>
            <sf:form class="form-horizontal" action="/feed" modelAttribute="feed" method='POST'>
                <div class="form-group">
                    <div class="col-md-5">
                        <label for="feed">Feed:</label>
                        <sf:textarea path="text" id="feed" type="text" class="form-control" placeholder="Post your feed" />
                    </div>
                </div>
                <div class="row">
                    <br/>
                    <div class="col-md-4">
                        <sf:button class="btn btn-lg btn-primary " type="submit">Post</sf:button>
                    </div>
                    <br/><br/><br/>
                </div>
            </sf:form>
        </div>
    </div>
</div>

</body>
</html>
