<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<sec:authorize var="loggedIn" access="isAuthenticated()"/>
<c:choose>
    <c:when test="${loggedIn}">
        <t:page>
          <jsp:body>
            <div class="page-header">
              <h3>Boards list</h3>
            </div>
              <div class="row">
                <div class="col-md-10">
                  <c:choose>
                    <c:when test="${not empty boards}">
                          <c:forEach items="${boards}" var="board">
                            <div class="panel panel-default">
                              <div class="panel-heading">
                                <div class="row">
                                  <div class="col-md-8">
                                      <h2 class="panel-title"><b>${board.boardName}</b></h2>
                                  </div>
                                  <div class="col-md-2">
                                      <a href="/user/${board.owner.id}"><c:out value="${board.owner.username}" /></a>
                                  </div>
                                  <div class="col-md-2 text-right">
                                      <fmt:formatDate pattern="dd/MM/yyyy HH:mm" value="${board.lastUpdateDate}" />
                                  </div>
                                </div>
                              </div>
                              <div class="panel-body">
                                <c:out value="${board.description}" escapeXml="false" />
                              </div>
                              <ul class="list-group">
                                <li class="list-group-item">
                                  <p class="pull-right"><a href="/board/${board.id}">See more...</a></p>
                                    <br/>
                                </li>
                              </ul>
                            </div>
                          </c:forEach>
                      </c:when>
                  <c:otherwise>
                    <p class="well">No boards found.</p>
                  </c:otherwise>
                </c:choose>
              </div>
            </div>
            </jsp:body>
        </t:page>
    </c:when>
    <c:otherwise>
        <%@include file="login.jsp" %>
    </c:otherwise>
</c:choose>
