<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<sec:authorize var="loggedIn" access="isAuthenticated()"/>
<c:choose>
    <c:when test="${loggedIn}">
        <t:page>
            <jsp:body>
                <br/>
                <br/>
                <div class="row">
                    <div class="col-md-10">
                        <c:choose>
                            <c:when test="${not empty notFound}">
                                <div class="alert alert-danger">
                                    No feed with this ID found.
                                </div>
                            </c:when>
                            <c:otherwise>
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        Posted by <a href="<c:url value="/user/${feed.owner.id}"/>">${feed.owner.username}</a>
                                        <p class="pull-right">
                                            <b>Added: </b><fmt:formatDate pattern="dd/MM/yyyy HH:mm" value="${feed.whenAdded}" />
                                        </p>
                                    </div>
                                    <div class="panel-body">
                                            ${feed.content}
                                    </div>
                                    <div class="panel-footer text-center">
                                        <b style="font-size: medium;">Comments</b>
                                    </div>
                                    <ul class="list-group">
                                        <c:if test="${empty comments}">
                                            <li class="list-group-item text-center">
                                                <b>No comments</b>
                                            </li>
                                        </c:if>
                                        <c:if test="${not empty comments}">
                                            <c:forEach var="comment" items="${comments}">
                                                <li class="list-group-item">
                                                    <b><a href="<c:url value="/user/${comment.commentAuthor.id}"/>">${comment.commentAuthor.username}</a>: </b>
                                                        ${comment.commentContent}
                                                </li>
                                            </c:forEach>
                                        </c:if>

                                        <c:if test="${empty notFound}">
                                            <sf:form class="form-horizontal" method="POST" modelAttribute="comment" action="/feed/${feed.id}">
                                                <li class="list-group-item">
                                                    <div class="row">
                                                        <div class="col-md-10">
                                                            <sf:input path="commentContent" id="commentContent" class="form-control"
                                                                      placeholder="Enter your comment here..."/>
                                                            <sf:errors path="commentContent" cssStyle="color: #ff0000"/>
                                                        </div>
                                                        <div class="col-sm-2 text-center">
                                                            <button type="submit" class="btn btn-md btn-success">Submit comment</button>
                                                        </div>
                                                    </div>
                                                </li>
                                            </sf:form>
                                        </c:if>
                                    </ul>
                                </div>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </jsp:body>
        </t:page>
    </c:when>
    <c:otherwise>
        <%@include file="login.jsp" %>
    </c:otherwise>
</c:choose>
