import controller.LoginController;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import repository.UserRepository;
import repository.UserRolesRepository;
import service.UserService;

@Configuration
public class TestContext {

    @Bean
    public UserRepository userRepository(){
        return Mockito.mock(UserRepository.class);
    }

    @Bean
    public UserRolesRepository userRolesRepository(){
        return Mockito.mock(UserRolesRepository.class);
    }

    @Bean
    public UserService userService() {
        UserService service = Mockito.mock(UserService.class);
        return service;
    }

    @Bean
    public LoginController loginController(){
        return new LoginController();
    }
}