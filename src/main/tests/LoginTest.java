import controller.LoginController;
import model.User;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.servlet.View;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestContext.class})
@WebAppConfiguration
public class LoginTest {
    @InjectMocks
    LoginController loginController;

    @Mock
    View mockView;

    MockMvc mockMvc;

    Authentication auth;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        auth = new UsernamePasswordAuthenticationToken("test", "test");
        SecurityContextHolder.getContext().setAuthentication(auth);
        this.mockMvc = standaloneSetup(loginController).setSingleView(mockView).build();
    }

    @Test
    public void testRedirectToLogin() throws Exception{
        this.mockMvc.perform(get("/")).andExpect(view().name("redirect:/login"));
    }

    @Test
    public void testLoginCorrect() throws Exception{
        this.mockMvc.perform(get("/").principal(auth)).andExpect(view().name("redirect:/welcome"));
    }

    @Test
    public void testLoginView() throws Exception {
        this.mockMvc.perform(get("/login")).andExpect(view().name("login"));
    }

    @Test
    public void loginFailedTest() throws Exception {
        this.mockMvc.perform(get("/loginfailed")).andExpect(model().attributeExists("error"));
    }

    @Test
    public void logoutTest() throws Exception {
        this.mockMvc.perform(get("/logout")).andExpect(model().attribute("logout", "true"));
    }

    @After
    public void tearDown() {
        SecurityContextHolder.clearContext();
    }
}
