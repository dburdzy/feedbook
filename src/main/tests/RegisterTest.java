import controller.RegisterController;
import model.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.servlet.View;
import service.UserService;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestContext.class})
@WebAppConfiguration
public class RegisterTest {
    @InjectMocks
    RegisterController registerController;

    @Mock
    View mockView;

    @Mock
    UserService userService;

    MockMvc mockMvc;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        this.mockMvc = standaloneSetup(registerController).setSingleView(mockView).build();
    }

    @Test
    public void testRegisterView() throws Exception {
        mockMvc.perform(get("/register")).andExpect(view().name("register"))
                .andExpect(model().attributeExists("user"));
    }

    @Test
    public void testRegisterFailed() throws Exception {
        String username = "test";
        when(userService.checkIfUserIsInDatabase(username)).thenReturn(true);
        mockMvc.perform(post("/register")
                .param("username", username))
                .andExpect(view().name("register"))
                .andExpect(model().attribute("addUserError", "true"));
    }

    @Test
    public void testRegisterUser() throws Exception {
        User user = new User();
        String username = "test";
        user.setUsername(username);
        when(userService.checkIfUserIsInDatabase(username)).thenReturn(false);
        mockMvc.perform(post("/register")
                .param("username", username))
                .andExpect(view().name("redirect:/"));
        verify(userService).insertNewUser(user);
    }
}
